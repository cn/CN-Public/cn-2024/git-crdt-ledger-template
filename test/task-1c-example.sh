#!/usr/bin/env bash
BIN=$1

if [ -z "$BIN" ] ; then
    echo "Provide path to crdt-ledger scripts" >&2
    exit 1
else
    cd $BIN
    BIN=$(pwd)
    cd -
fi

echo "Creating Replicas"
$BIN/repo-init alice
$BIN/repo-init bob
$BIN/repo-init carol
echo ""

echo "Establishing Full Topology for Fast Bootstrap"
cd alice
git remote add bob ../bob
git remote add carol ../carol
cd ../bob
git remote add alice ../alice
git remote add carol ../carol
cd ../carol
git remote add bob ../bob
git remote add alice ../alice
echo ""

echo "Creating a new ledger"
cd ../alice
LDGID=$($BIN/ledger-init)

# Three rounds of broadcast and repo-sync are need
# to fully initalize all append-only logs and
# all counters on all replicas
echo "Creating accounts and initializing counters"
for I in $(seq 3); do 
    echo "Round $I"
    cd ../alice
    $BIN/repo-sync
    git push bob "refs/heads/ledgers/*:refs/remotes/alice/ledgers/*"
    git push carol "refs/heads/ledgers/*:refs/remotes/alice/ledgers/*"
    echo ""

    cd ../bob
    $BIN/repo-sync
    git push alice "refs/heads/ledgers/*:refs/remotes/bob/ledgers/*"
    git push carol "refs/heads/ledgers/*:refs/remotes/bob/ledgers/*"

    cd ../carol
    $BIN/repo-sync
    git push bob "refs/heads/ledgers/*:refs/remotes/carol/ledgers/*"
    git push alice "refs/heads/ledgers/*:refs/remotes/carol/ledgers/*"
done


echo "Downgrading to Transitively-Connected Topology for Test"
cd ../alice
git remote rm carol
cd ../carol
git remote rm alice
echo ""

echo "Starting test"
echo "TODO"
echo "Test completed"

# Final state of all replicas:
# balance should be correct and
# all counters should be consistent
# on all replicas.
cd ../alice
$BIN/ledger-monitor $LDGID 0 3
echo ""

cd ../bob
$BIN/ledger-monitor $LDGID 0 3
echo ""

cd ../carol
$BIN/ledger-monitor $LDGID 0 3
echo ""
